# Better Icon
Better icon will set the raid target icon for the players target instead of  
toggling it like the WoW API `SetRaidTargetIcon` does.  
What this means is, if you ran `SetRaidTargetIcon` on a target who already has  
an icon, they will have their icon removed. With BetterIcon, the icon will stay  
the same.

# Installation
1. Download this repository.
2. Place the extracted folder under your Addons directory.

# Example Usage
Setting the star icon for the players current target:
```
/bi 1
```