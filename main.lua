 _G.SLASH_BETTER1 = '/bi'
 function SlashCmdList.BETTER(command) 
  if command == 'help' then
    return help()
  end
  icon = GetRaidTargetIndex("target")
  if command == tostring(icon) then
    return
  end
  SetRaidTargetIcon("target", command)
 end

 function help() 
    print('Usage:')
    print('/bi ICON_NUMBER')
    print('Icon Numbers:')
    print('1 - {Star}')
    print('2 - {Circle}')
    print('3 - {Diamond}')
    print('4 - {Triangle}')
    print('5 - {Moon}')
    print('6 - {Square}')
    print('7 - {Cross}')
    print('8 - {Skull}')
 end